#!/bin/bash

docker run --name ${USER}_${IMAGE_NAME} -d --gpus all -p 8900:8888 -v /home/${USER}:/wd ${USER}_${IMAGE_NAME}
