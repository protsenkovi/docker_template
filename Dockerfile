FROM conda/miniconda3

ARG USER
ARG GROUP
ARG UID
ARG GID

RUN groupadd --gid ${GID} ${GROUP}
RUN useradd --shell /bin/bash --uid ${UID} --gid ${GID} --create-home ${USER}
RUN mkdir /wd
RUN chown ${USER}:${GROUP} /wd

# SYSTEM INITIALIZATION

USER ${USER}

# USER INITIALIZATION

SHELL ["/bin/bash", "--login", "-i", "-c"]
WORKDIR /wd
