#!/bin/bash

docker build . -t ${USER}_${IMAGE_NAME} \
	--build-arg USER=${USER} \
	--build-arg GROUP=${USER} \
	--build-arg UID=$(id -u ${USER}) \
	--build-arg GID=$(id -g ${USER})
