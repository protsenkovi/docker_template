#!/bin/bash

docker run -it --rm --gpus all -p 8900:8888 -v /home/${USER}:/wd ${USER}_${IMAGE_NAME}
